# PgAdvisoryLock

PgAdvisoryLock is a PostgreSQL specific feature designed to wrap the [Advisory
Lock][1] features of the database. It's a good idea to read that information,
but the basic overview from that page is:

> PostgreSQL provides a means for creating locks that have application-defined
> meanings. These are called advisory locks, because the system does not
> enforce their use — it is up to the application to use them correctly.
> Advisory locks can be useful for locking strategies that are an awkward fit
> for the MVCC model. For example, a common use of advisory locks is to
> emulate pessimistic locking strategies typical of so called "flat file" data
> management systems. While a flag stored in a table could be used for the
> same purpose, advisory locks are faster, avoid table bloat, and are
> automatically cleaned up by the server at the end of the session.

PgAdvisoryLock allows you to simply call

    PgAdvisoryLock.synchronise(:a_meaningful_symbol) do
      # something important that must not run with itself concurrently
    end

and the code in the block will be executed once and once only at a time.
`synchronise` is aliased to `synchronize`.

## Why would I want to do this?

PgAdvisoryLock came out of [PaymentHub][3]'s application, as a way of ensuring
that certain tasks are not run twice. At the beginning of the job, it checks if
it has already completed, but if there is another instance that has not finished
yet then we may get double-ups, which is not ideal. This way uses a feature from
a system already in use (PostgreSQL) to meet our needs without trouble.

## How do I use it?

### Step 1a: Configure your symbols

The simplest way to use this feature is to just use integers

    PgAdvisoryLock.synchronise(1)

Unfortunately this means you have to remember what the numbers are for and make
sure you get it right across the board. Also, if you use:

    PgAdvisoryLock.show_locks

you'll just get the numbers

Instead, define some symbols that have meaning and you'll be able to see those
symbols if you interrogate `show_locks`. If you're using Rails, put this in an
initialiser

    # config/initializers/pg_advisory_lock_init.rb
    PgAdvisoryLock.symbols = :the_first_important_thing, :another_important_thing

When you call it when locks are in use, you'll get this sort of output:

    > PgAdvisoryLock.show_locks
    => [{:the_first_important_thing => nil}]

(I'll explain the output later)

It's important not to change the order of the symbols as under the hood they are
just an integer representation of a position in an array. If you need to change
the order or add/remove some, make sure no locks are in use at the time.

### Step 1b: Optional - configure your logger

If you're using Rails and you want to use a logger other than `Rails.logger`,
then configure it like so:

    # config/initializers/pg_advisory_lock_init.rb
    PgAdvisoryLock.logger = SomeOther.logger

If you're not using Rails and you want to use a logger other than `Logger.new(STDOUT)`,
likewise update it in your initialisation.

### Step 1b: Optional - choose a lock type

If, for some reason, you don't want the gem to choose a lock type for you, you
can choose between the `XactLock` and `SessionLock` mechanisms manually like so:

    # config/initializers/pg_advisory_lock_init.rb
    PgAdvisoryLock.lock_with :xact_lock
    # or
    PgAdvisoryLock.lock_with :session_lock

This is a class-level configuration which will alter behaviour globally, so only
set it once in your initialiser!

If you choose to call a lock class directly (as shown in the next step), this is
ignored. That's probably a better choice if you want this behaviour

### Step 2: Start using it

It's easy to use, just wrap your existing code in a synchronise call:

    PgAdvisoryLock.synchronise(:symbol1) do
      # your existing code
    end

By default, PgAdvisoryLock chooses whether to use the ['transaction lock'][2]
functionality that became available in PostgreSQL 9.1. Prior to that, locks had
to be explicitly acquired and released. If you wish to use a particular
implementation, you may do so very easily:

    # Use transactional locking
    PgAdvisoryLock::XactLock.synchronise(:symbol1) do
      # your existing code
    end

    # Use session locking
    PgAdvisoryLock::SessionLock.synchronise(:symbol1) do
      # your existing code
    end

The API is the same for both.

### Step 3: There is no Step 3

## synchronise API

The main method `synchronise` takes the following arguments

    synchronise(sym, options = {})

`sym` is the symbol or ID you have defined previously.
`options` takes the following parameters:

* :connection - if you want to provide a different ActiveRecord connection
  object. Mainly used in tests.
* :instance_id - if you provide this the lock is acquired on both the first
  parameter and this id. Note that matches on lock like this are exact.
  The implication of this is if you lock on `(:symbol)` you can also lock
  on `(:symbol, :instance_id => 1)` at the same time without conflict.
* :shared - invokes the "shared lock" variants of the [functions][2].

Where an `:instance_id` is specified, when `show_locks` is called, the
`:instance_id` goes into the results hash like so:

    PgAdvisoryLock.synchronise(:symbol1, :instance_id => 1000) do
      PgAdvisoryLock.show_locks
    end
    => [{:symbol1=>1000}]


## Exceptions

If a lock cannot be acquired, `PgAdvisoryLock::LockUnavailable` will be raised

If a symbol is passed as the first argument but is not defined beforehand,
`PgAdvisoryLock::InvalidKey` will be raised. Although symbols can be converted
to integers by the ruby runtime, they are not consistent from one execution to
the next, so we do not try.

[1]: http://www.postgresql.org/docs/9.1/interactive/explicit-locking.html#ADVISORY-LOCKS
[2]: http://www.postgresql.org/docs/9.2/interactive/functions-admin.html#FUNCTIONS-ADVISORY-LOCKS
[3]: http://www.paymenthub.com.au


## Running the tests

* Make sure you update the database.yml to your local PostgreSQL instance.
* Run `rake spec`
