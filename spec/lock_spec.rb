
describe PgAdvisoryLock::Base do
  before(:each) do
    @conn1 = Connection1.connection
    @conn2 = Connection2.connection
    @sym = :symbol1
    PgAdvisoryLock.symbols = [:symbol1, :symbol2, :symbol3]
  end

  [PgAdvisoryLock::XactLock, PgAdvisoryLock::SessionLock].each do |klass|
    it "#{klass} should lock exclusively (no parallelism on different connections)" do
      PgAdvisoryLock.show_locks(@conn1).should == []
      PgAdvisoryLock.show_locks(@conn2).should == []
      
      # Because we can't reliably test with threads, we can use the block on different
      # connections. Within the block the lock is held, and different connections to
      # the DB simulates the behaviour of concurrent processes.
      klass.synchronise(@sym, :connection => @conn1) do
        PgAdvisoryLock.show_locks(@conn1).should == [{@sym => nil}]
        PgAdvisoryLock.show_locks(@conn2).should == [{@sym => nil}]
        lambda {
          klass.synchronise(@sym, :connection => @conn2) do
          end
        }.should raise_error(PgAdvisoryLock::LockUnavailable)
      end

      klass.synchronise(@sym, :instance_id => 1, :connection => @conn1) do
        PgAdvisoryLock.show_locks(@conn1).should == [{@sym => 1}]
        PgAdvisoryLock.show_locks(@conn2).should == [{@sym => 1}]
        lambda {
          klass.synchronise(@sym, :instance_id => 1, :connection => @conn2) do
          end
        }.should raise_error(PgAdvisoryLock::LockUnavailable)
        # Note that with and wihout instance_id are separate locks
        lambda {
          klass.synchronise(@sym, :connection => @conn2) do
          end
        }.should_not raise_error(PgAdvisoryLock::LockUnavailable)
      end
      
      PgAdvisoryLock.show_locks(@conn1).should == []
      PgAdvisoryLock.show_locks(@conn2).should == []
    end

    it "#{klass} should complain if there is a conflict, even if it's nested" do
      PgAdvisoryLock.show_locks(@conn1).should == []
      PgAdvisoryLock.show_locks(@conn2).should == []

      @sym.should_not == :symbol2
      @sym.should_not == :symbol3

      # Because we can't reliably test with threads, we can use the block on different
      # connections. Within the block the lock is held, and different connections to
      # the DB simulates the behaviour of concurrent processes.
      klass.synchronise(@sym, :connection => @conn1) do
        PgAdvisoryLock.show_locks(@conn1).should == [{@sym => nil}]
        PgAdvisoryLock.show_locks(@conn2).should == [{@sym => nil}]
        lambda {
          klass.synchronise(:symbol2, :connection => @conn2) do
            klass.synchronise(:symbol3, :connection => @conn2) do
            end
          end
        }.should_not raise_error(PgAdvisoryLock::LockUnavailable)
        lambda {
          klass.synchronise(:symbol2, :connection => @conn2) do
            klass.synchronise(@sym, :connection => @conn2) do
            end
          end
        }.should raise_error(PgAdvisoryLock::LockUnavailable)
      end

      PgAdvisoryLock.show_locks(@conn1).should == []
      PgAdvisoryLock.show_locks(@conn2).should == []
    end

    it "#{klass} should allow serialisation" do
      PgAdvisoryLock.show_locks(@conn1).should == []
      PgAdvisoryLock.show_locks(@conn2).should == []
      
      klass.synchronise(@sym, :connection => @conn1) do
        PgAdvisoryLock.show_locks(@conn1).should == [{@sym => nil}]
        PgAdvisoryLock.show_locks(@conn2).should == [{@sym => nil}]
      end
      klass.synchronise(@sym, :connection => @conn2) do
        PgAdvisoryLock.show_locks(@conn1).should == [{@sym => nil}]
        PgAdvisoryLock.show_locks(@conn2).should == [{@sym => nil}]
      end
      
      PgAdvisoryLock.show_locks(@conn1).should == []
      PgAdvisoryLock.show_locks(@conn2).should == []
    end
    
    it "#{klass} should allow nesting on the same connection" do
      PgAdvisoryLock.show_locks(@conn1).should == []
      PgAdvisoryLock.show_locks(@conn2).should == []
      
      klass.synchronise(@sym, :connection => @conn1) do
        PgAdvisoryLock.show_locks(@conn1).should == [{@sym => nil}]
        PgAdvisoryLock.show_locks(@conn2).should == [{@sym => nil}]
        klass.synchronise(@sym, :connection => @conn1) do
          # Note that because we have it, it's listed only once per conn
          PgAdvisoryLock.show_locks(@conn1).should == [{@sym => nil}]
          PgAdvisoryLock.show_locks(@conn2).should == [{@sym => nil}]
        end
        PgAdvisoryLock.show_locks(@conn1).should == [{@sym => nil}]
        PgAdvisoryLock.show_locks(@conn2).should == [{@sym => nil}]
      end
      
      PgAdvisoryLock.show_locks(@conn1).should == []
      PgAdvisoryLock.show_locks(@conn2).should == []
    end
    
    it "#{klass} should pass out errors" do
      PgAdvisoryLock.show_locks(@conn1).should == []
      PgAdvisoryLock.show_locks(@conn2).should == []
      
      message = "This is not an #{klass} error"
      lambda {
        klass.synchronise(@sym, :connection => @conn1) do
          raise message
        end
      }.should raise_error(RuntimeError, message)
      
      PgAdvisoryLock.show_locks(@conn1).should == []
      PgAdvisoryLock.show_locks(@conn2).should == []
    end
    
    it "#{klass} should reject invalid symbols" do
      lambda {
        klass.synchronise(:i_dont_exist) do
        end
      }.should raise_error(PgAdvisoryLock::InvalidKey)
      PgAdvisoryLock.show_locks.should == []
      
      lambda {
        klass.synchronise(@sym) do
        end
      }.should_not raise_error(PgAdvisoryLock::InvalidKey)
      PgAdvisoryLock.show_locks.should == []

      lambda {
        klass.synchronise(50) do
        end
      }.should_not raise_error(PgAdvisoryLock::InvalidKey)
      PgAdvisoryLock.show_locks.should == []

      message = "One BILLION Dollars"
      result = klass.synchronise(@sym) do
        message
      end
      result.should == message
    end

  end
end
