require 'rspec'
require 'pg_advisory_lock'

#dbconfig = YAML::load(File.open('database.yml'))
ActiveRecord::Base.establish_connection(YAML::load(File.open('database.yml')))

class ::Connection1 < ActiveRecord::Base
  self.abstract_class = true
  establish_connection(YAML::load(File.open('database.yml')))
end
class ::Connection2 < ActiveRecord::Base
  self.abstract_class = true
  establish_connection(YAML::load(File.open('database.yml')))
end

RSpec.configure do |config|
  config.color_enabled = true
  config.formatter     = 'documentation'
  config.mock_with :rspec
end
