module PgAdvisoryLock
  class Boolean
    def self.parse(str)
      %w(t).include?(str)
    end
  end
end
