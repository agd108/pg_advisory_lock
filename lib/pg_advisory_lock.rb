require 'logger'
require 'active_record'
require "pg_advisory_lock/version"
require "pg_advisory_lock/boolean"
require "pg_advisory_lock/xact_lock"
require "pg_advisory_lock/session_lock"

module PgAdvisoryLock

  class InvalidKey < StandardError; end
  class LockUnavailable < StandardError; end


  @@logger = nil
  @@symbols = []
  @@offset = 100

  class << self
    def version_string
      VERSION
    end

    def symbols=(*arr)
      @@symbols = Array(*arr)
    end
    def symbols
      @@symbols
    end

    def id_for_symbol(sym)
      sym_id = @@symbols.index(sym)
      sym_id += self.offset if sym_id
    end
    #protected :id_for_symbol
    def symbol_for_id(id)
      sym_id = id
      sym_id -= self.offset if sym_id
      @@symbols.at(sym_id)
    end
    #protected :symbol_for_id

    def logger=(logger)
      @@logger = logger
    end
    def logger
      return @@logger if @@logger
      @@logger ||= Rails.logger if defined?(Rails)
      @@logger ||= Logger.new(STDOUT)
      @@logger
    end

    def offset=(offset)
      @@offset = offset
    end
    def offset
      @@offset
    end

    @@server_version = nil
    def server_version(connection = ActiveRecord::Base.connection)
      @@server_version ||= connection.send(:postgresql_version)
    end

    @@lock_with = nil
    def lock_with(sym)
      if sym == :xact_lock
        @@lock_with = XactLock
      elsif sym == :session_lock
        @@lock_with = SessionLock
      else
        raise NotImplementedError
      end
    end

    def locktype
      if !@@lock_with.nil?
        @@lock_with
      elsif server_version > 901000
        @@lock_with = XactLock
      else
        @@lock_with = SessionLock
      end
    end

    def synchronise(*args, &blk)
      locktype.synchronise(*args, &blk)
    end
    alias_method :synchronize, :synchronise

    def show_locks(connection = ActiveRecord::Base.connection)
      rows = connection.select_rows(
        "SELECT locktype, classid, objid, pid, mode, granted FROM pg_locks WHERE locktype = 'advisory'")
      rows.collect do |row|
        locktype, classid, objid, pid, mode, granted = *row
        granted = Boolean.parse(granted)

        classid = classid.to_i
        objid = objid.to_i

        # If the advisory lock was created with just one ID parameter, then that
        # value is stored in the objid. However, if there are 2 params, the one
        # that would have otherwise been found in objid (that is, the first
        # one), is actually in classid, with the second param in objid.
        sym = nil; instance_id = nil; id = nil
        if classid > 0
          # then both classid and objid are in use
          id = classid
          instance_id = objid
        else
          id = objid
        end

        sym = symbol_for_id(id)
        granted ? {(sym || id) => instance_id} : nil
      end.compact
    end

    def locked?(sym, options = {})
      # returns true/false based on whether a lock has been acquired for a given symbol / instance_id.

      sym = sym.to_sym if sym.is_a?(String)
      connection = options[:connection] || ActiveRecord::Base.connection
      instance_id = options[:instance_id]

      locks = show_locks(connection)

      if instance_id.present?
        locks.any? {|hsh| hsh.has_key?(sym) and hsh[sym].to_s == instance_id.to_s }
      else
        locks.any? {|hsh| hsh.has_key?(sym)}
      end
    end
  end
end
